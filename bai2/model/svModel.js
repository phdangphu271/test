const SinhVien = function (ma, ten, email, matKhau, toan, ly, hoa) {
  //left: keys
  this.ma = ma;
  this.ten = ten;
  this.email = email;
  this.matKhau = matKhau;
  this.toan = toan;
  this.ly = ly;
  this.hoa = hoa;

  // this.calcAvg = function (toan, ly, hoa) {
  //   return (this.toan + this.ly + this.hoa) / 3;
  // };
};

// prototypal inheritance
SinhVien.prototype.calcAvg = function (toan, ly, hoa) {
  return (this.toan + this.ly + this.hoa) / 3;
};

// in es6 classes , we use function expression

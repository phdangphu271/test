const maEl = document.getElementById("txtMaSV");
const tenEl = document.getElementById("txtTenSV");
const emailEl = document.getElementById("txtEmail");
const matKhauEl = document.getElementById("txtPass");
const diemToanEl = document.getElementById("txtDiemToan");
const diemLyEl = document.getElementById("txtDiemLy");
const diemHoaEl = document.getElementById("txtDiemHoa");

function layThongTinTuSv() {
  const maSv = maEl.value;
  const tenSv = tenEl.value;
  const email = emailEl.value;
  const matKhau = matKhauEl.value;
  const diemToan = +diemToanEl.value;
  const diemLy = +diemLyEl.value;
  const diemHoa = +diemHoaEl.value;

  const sv = new SinhVien(
    maSv,
    tenSv,
    email,
    matKhau,
    diemToan,
    diemLy,
    diemHoa
  );

  return sv;
}

function renderDssv(svArr) {
  console.log(svArr);
  let contentHTML = "";

  svArr.forEach((sv) => {
    console.log(sv.__proto__);
    const avgScore = sv.calcAvg(sv.toan, sv.ly, sv.hoa);
    contentHTML += `
    <tr>
      <td>${sv.ma}</td>
      <td>${sv.ten}</td>
      <td>${sv.email}</td>
      <td>${avgScore.toFixed(2)}</td>
      <td>
      <button onclick="xoaSv('${sv.ma}')" class="btn btn-danger">Xóa</button>
      <button onClick="suaSv('${sv.ma}')" class="btn btn-warning">Sửa</button>
      </td>   
    </tr>`;
  });

  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

const timKiemViTri = (id, svArr) => {
  for (let i = 0; i < svArr.length; i++) {
    const item = svArr[i];
    if (item.ma === id) {
      return i;
    }
  }
  return -1;
};

const showThongTinLenForm = (obj) => {
  console.log(obj);
  maEl.value = obj.ma;
  tenEl.value = obj.ten;
  emailEl.value = obj.email;
  matKhauEl.value = obj.matKhau;
  diemToanEl.value = obj.toan;
  diemLyEl.value = obj.ly;
  diemHoaEl.value = obj.hoa;
};

const showMessageErr = (idErr, message) => {
  document.getElementById(idErr).innerHTML = message;
};

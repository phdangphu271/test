const kiemTraTrung = (sv, dssv) => {
  let i = timKiemViTri(sv.ma, dssv);

  if (i !== -1) {
    showMessageErr("spanMaSV", "Ma sinh vien da ton tai");
    return false;
  } else {
    showMessageErr("spanMaSV", "");
    return true;
  }
};

const kiemTraRong = (userInput, idErr, message) => {
  if (userInput.length === 0) {
    showMessageErr(idErr, message);
    return false;
  } else {
    showMessageErr(idErr, "");
    return true;
  }
};

const kiemTraSo = (value, idErr, message) => {
  var reg = /^\d+$/;

  let isNumber = reg.test(value);

  if (isNumber) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
};

const kiemTraEmail = (value, idErr) => {
  var reg =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  let isEmail = reg.test(value);

  if (isEmail) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, "email khong dung dinh dang");
    return false;
  }
};

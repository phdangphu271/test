const dssv = [];

const DSSV = "DSSV";

// lay du lieu tu localstorage khi user load trang
// localstorage.getitem: lay du lieu tu local
const luuLocalStorage = () => {
  const jsonDssv = JSON.stringify(dssv);
  localStorage.setItem(DSSV, jsonDssv);
};

const dataJson = localStorage.getItem(DSSV);

// console.log("dataJson:", dataJson);

if (dataJson !== null) {
  console.log("in the if statement, data loaded from localstorage");
  const initialSvArr = JSON.parse(dataJson);

  initialSvArr.forEach((sv) => {
    // console.log(sv.__proto__);
    const newSv = new SinhVien(
      sv.ma,
      sv.ten,
      sv.email,
      sv.matKhau,
      sv.toan,
      sv.ly,
      sv.hoa
    );
    dssv.push(newSv);
    console.log(dssv);
    console.log(newSv.__proto__);
  });
  renderDssv(dssv);
}

const themSv = () => {
  const sv = layThongTinTuSv();
  console.log(sv.__proto__);

  let isValid = true;
  isValid =
    kiemTraTrung(sv, dssv) &&
    kiemTraRong(sv.ma, "spanMaSV", "ma sinh vien khong duoc de rong") &&
    kiemTraSo(sv.ma, "spanMaSV", "Ma sinh vien phai la so");

  isValid =
    isValid &
    kiemTraRong(sv.ten, "spanTenSV", "ten sinh vien khong duoc de rong");

  isValid = isValid & kiemTraEmail(sv.email, "spanEmailSV");

  if (isValid) {
    dssv.push(sv);

    console.log(dssv);

    luuLocalStorage();

    renderDssv(dssv);
  }
};

const xoaSv = (id) => {
  console.log(id);

  const viTri = timKiemViTri(id, dssv);
  if (viTri !== -1) {
    dssv.splice(viTri, 1);

    luuLocalStorage();

    renderDssv(dssv);
  }
};

const suaSv = (id) => {
  const viTri = timKiemViTri(id, dssv);
  if (viTri === -1) return;

  const data = dssv[viTri];

  showThongTinLenForm(data);
  console.log(data);

  document.getElementById("txtMaSV").disabled = true;
};

const capNhatSv = () => {
  const data = layThongTinTuSv();
  console.log(data);
  const viTri = timKiemViTri(data.ma, dssv);
  if (viTri === -1) return;

  dssv[viTri] = data;
  renderDssv(dssv);
  luuLocalStorage();

  document.getElementById("txtMaSV").disabled = false;
  resetForm();
};

const resetForm = () => {
  document.getElementById("formQLSV").reset();
};
